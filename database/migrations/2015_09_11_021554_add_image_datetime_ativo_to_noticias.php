<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddImageDatetimeAtivoToNoticias extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('noticias', function(Blueprint $table)
		{
			$table->text('image')->nullable();
			$table->boolean('ativo')->nullable()->default(1);
			$table->datetime('datetime')->nullable();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('noticias', function(Blueprint $table)
		{
			$table->dropColumn('image');
			$table->dropColumn('ativo');
			$table->dropColumn('datetime');
		});
	}

}
