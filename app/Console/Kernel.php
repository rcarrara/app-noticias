<?php namespace App\Console;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel {

	/**
	 * The Artisan commands provided by your application.
	 *
	 * @var array
	 */
	protected $commands = [
		'App\Console\Commands\Inspire',
		'App\Console\Commands\CrawlerG1',
		'App\Console\Commands\CrawlerJcnet',
		'App\Console\Commands\CrawlerSocial',
		'App\Console\Commands\Crawler94fm',
		'App\Console\Commands\Crawler96fm',
	];

	/**
	 * Define the application's command schedule.
	 *
	 * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
	 * @return void
	 */
	protected function schedule(Schedule $schedule)
	{
		$schedule->command('crawler:g1')
				 ->everyThirtyMinutes();

		$schedule->command('crawler:jcnet')
				 ->everyThirtyMinutes();

		$schedule->command('crawler:social')
				 ->everyThirtyMinutes();

		//$schedule->command('crawler:94fm')
		//		 ->everyThirtyMinutes();

		$schedule->command('crawler:96fm')
				 ->everyThirtyMinutes();
	}

}
