<?php namespace App\Console\Commands;

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;
use Goutte\Client;
use Log;

class CrawlerJcnet extends Command {

	/**
	 * The console command name.
	 *
	 * @var string
	 */
	protected $name = 'crawler:jcnet';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'Command description.';

	/**
	 * Create a new command instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		parent::__construct();
	}

	/**
	 * Execute the console command.
	 *
	 * @return mixed
	 */
	public function fire()
	{
		$client = new Client();

		$crawler = $client->request('GET', 'http://www.jcnet.com.br/ultimas.php');
		$noticias = array(); 
		$i = 0;
		$data = ''; 

		//datetime
		//image
		$array = $crawler->filter('table')->eq(7)->children()->each(function ($node) use ( &$noticias , &$i, &$data ){
			if (count($node->children()) == 1) {
				if ($node->children()->attr('valign') == 'bottom') {
				} elseif ($node->children()->attr('bgcolor') == '#E1ECFF') {
					$data = $node->children()->text();
				}
			} elseif (count($node->children()) == 3){
				$node->children()->each(function($td) use ( &$noticias, &$i, $data ){
					if ($td->attr('width') == '5%') {
						if (isSet($data)) {
							// $noticias[$i]['data'] = $data;
							$data = substr($data, 0, 8);
							$noticias[$i]['datetime'] = \Carbon\Carbon::createFromFormat('d/m/y H:i',$data.' '.$td->text());
						}
						// $noticias[$i]['hora'] = $td->text();
					} elseif ($td->attr('width') == '15%') {
						$noticias[$i]['categoria'] = $td->text();
					} elseif ($td->attr('width') == '80%') {
						$noticias[$i]['titulo'] = $td->text();
						$noticias[$i]['link'] = 'http://www.jcnet.com.br'.$td->children()->attr('href');
					}						
				});
				$noticias[$i]['site_id'] = 1;
				$i++;
			}
		});

		foreach ($noticias as $noticia) {
			$n = \App\Noticia::where('titulo',$noticia['titulo'])->get();
			if (!count($n)) {
				\App\Noticia::create($noticia);
			}
		}

		Log::info('Rodou crawler JCNET');

		$this->info("Crawler JCNET");
	}

	/**
	 * Get the console command arguments.
	 *
	 * @return array
	 */
	protected function getArguments()
	{
		return [
			// ['example', InputArgument::REQUIRED, 'An example argument.'],
		];
	}

	/**
	 * Get the console command options.
	 *
	 * @return array
	 */
	protected function getOptions()
	{
		return [
			// ['example', null, InputOption::VALUE_OPTIONAL, 'An example option.', null],
		];
	}

}
