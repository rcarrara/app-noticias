<?php namespace App\Console\Commands;

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;
use Goutte\Client;
use \App\Noticia;
use Log;

class Crawler94fm extends Command {

	/**
	 * The console command name.
	 *
	 * @var string
	 */
	protected $name = 'crawler:94fm';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'Command description.';

	/**
	 * Create a new command instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		parent::__construct();
	}

	/**
	 * Execute the console command.
	 *
	 * @return mixed
	 */
	public function fire()
	{
		$client = new Client;

		$crawler = $client->request(
			'GET', 'http://94fm.com.br/ultimas-noticias/'
			);

		$arr_noticias = []; $i = 0;
		$crawler->filter('div#listaultimas ul')->children()->first()->children()->each(function ($node) use (&$arr_noticias, &$i){
		   	$arr_noticias[$i]['datetime'] = \Carbon\Carbon::createFromFormat('d/m/Y H:i', substr($node->filter('p.titulo span')->text(), 1, -1) .' '. date("H:i"));
		    $arr_noticias[$i]['link'] = $node->filter('p.titulo a')->attr('href'); //link
		    $arr_noticias[$i]['titulo'] = $node->filter('p')->eq(1)->text()."<br>"; //titulo
		    $arr_noticias[$i]['site_id'] = 2; //titulo
		    $i++;
		});

		foreach ($arr_noticias as $noticia) {
			$n = Noticia::where('titulo',$noticia['titulo'])->count();
			if ($n == 0) {
				Noticia::create($noticia);
			}
		}
		Log::info('Rodou crawler 94fm');
		$this->info("Crawler 94fm Bauru");
	}

	/**
	 * Get the console command arguments.
	 *
	 * @return array
	 */
	protected function getArguments()
	{
		return [
			// ['example', InputArgument::REQUIRED, 'An example argument.'],
		];
	}

	/**
	 * Get the console command options.
	 *
	 * @return array
	 */
	protected function getOptions()
	{
		return [
			// ['example', null, InputOption::VALUE_OPTIONAL, 'An example option.', null],
		];
	}

}
