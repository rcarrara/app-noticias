<?php namespace App\Console\Commands;

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;
use Goutte\Client;
use Log;

class CrawlerSocial extends Command {

	/**
	 * The console command name.
	 *
	 * @var string
	 */
	protected $name = 'crawler:social';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'Command description.';

	/**
	 * Create a new command instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		parent::__construct();
	}

	/**
	 * Execute the console command.
	 *
	 * @return mixed
	 */
	public function fire()
	{	

		$client = new Client();

		$crawler = $client->request(
			'GET', 'http://socialbauru.com.br/'
		);

		$arr_noticias = []; $i = 0; $data = '';

		$crawler->filter('div.social_noticias')->children()->each(function ($node) use ( &$arr_noticias , &$i, &$data ){
			if ($node->filter('div')->count()) {
				if ($node->text() == 'hoje') {
					$data = \Carbon\Carbon::now()->format('d/m/Y');
				} else {
					$data = $node->text();
				}
			} elseif ($node->filter('ul')->count()) {
				$node->filter('ul')->children()->each(function($li) use ( &$arr_noticias , &$i, &$data) {
					$arr_noticias[$i]['titulo'] = $li->children()->text();
					$arr_noticias[$i]['link'] = $li->children()->attr('href');
					$arr_noticias[$i]['datetime'] = \Carbon\Carbon::createFromFormat('d/m/Y H:i', $data .' '. date("H:i"));
					$arr_noticias[$i]['site_id'] = 5;
					$i++;
				});
			}
		});

		foreach ($arr_noticias as $noticia) {
			$n = \App\Noticia::where('titulo',$noticia['titulo'])->count();
			if ($n == 0) {
				\App\Noticia::create($noticia);
			}
		}

		Log::info('Rodou crawler Social Bauru');

		$this->info("Crawler Social Bauru");
	}

	/**
	 * Get the console command arguments.
	 *
	 * @return array
	 */
	protected function getArguments()
	{
		return [
			// ['example', InputArgument::REQUIRED, 'An example argument.'],
		];
	}

	/**
	 * Get the console command options.
	 *
	 * @return array
	 */
	protected function getOptions()
	{
		return [
			// ['example', null, InputOption::VALUE_OPTIONAL, 'An example option.', null],
		];
	}

}
