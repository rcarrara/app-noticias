<?php namespace App\Console\Commands;

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;
use Goutte\Client;
use App\Noticia;
use Carbon\Carbon;
use Log;
use DB;

class Crawler96fm extends Command {

	protected $client;
	protected $noticia;
	/**
	 * The console command name.
	 *
	 * @var string
	 */
	protected $name = 'crawler:96fm';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'Command description.';

	/**
	 * Create a new command instance.
	 *
	 * @return void
	 */
	public function __construct(Noticia $noticia, Client $client)
	{
		$this->noticia = $noticia;
		$this->client = $client;		
		parent::__construct();
	}

	/**
	 * Execute the console command.
	 *
	 * @return mixed
	 */
	public function fire()
	{
		$crawler = $this->client->request(
			'GET', 'http://www.96fmbauru.com.br/noticias/Ultimas-Noticias'
		);

		$arrayNoticias = [];
		$i = 0;
		$data = '';

		$crawler->filter('div.noticia')->children()->each(function ($node) use (&$arrayNoticias, &$i, &$data) {
			if ($node->attr('class') == 'font14 fontPreto fontBold') {
				$data = $node->text();
			} elseif ($node->attr('class') == '') {
				$arrayNoticias[$i]['titulo'] = $node->text();
				$arrayNoticias[$i]['link'] = 'http://www.96fmbauru.com.br' . $node->children()->attr('href');
				$arrayNoticias[$i]['datetime'] = Carbon::createFromFormat('d/m/y H:i', $data .' '. date("H:i"));
				$arrayNoticias[$i]['site_id'] = 6;
				$i++;
			} 
		});

		DB::beginTransaction();

		foreach ($arrayNoticias as $noticia) {
			$n = $this->noticia->where('titulo',$noticia['titulo'])->count();
			if ($n == 0) {
				$this->noticia->create($noticia);
			}
		}

		DB::commit();

		Log::info('Rodou crawler 96fm');
		$this->info("Crawler 96fm Bauru");
	}

	/**
	 * Get the console command arguments.
	 *
	 * @return array
	 */
	protected function getArguments()
	{
		return [
			// ['example', InputArgument::REQUIRED, 'An example argument.'],
		];
	}

	/**
	 * Get the console command options.
	 *
	 * @return array
	 */
	protected function getOptions()
	{
		return [
			// ['example', null, InputOption::VALUE_OPTIONAL, 'An example option.', null],
		];
	}

}
