<?php namespace App\Console\Commands;

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;
use DB;
use Log;

class CrawlerG1 extends Command {

	/**
	 * The console command name.
	 *
	 * @var string
	 */
	protected $name = 'crawler:g1';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'Command description.';

	/**
	 * Create a new command instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		parent::__construct();
	}

	/**
	 * Execute the console command.
	 *
	 * @return mixed
	 */
	public function fire()
	{
		$ch = curl_init();
	    curl_setopt($ch, CURLOPT_URL, 'http://g1.globo.com/dynamo/plantao/sp/bauru-marilia/1.json');
	    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); 
		$output = curl_exec($ch);
		curl_close($ch);


		$obj_output = json_decode($output);
		$arr_noticias = [];

		sleep(10);

		$i = 0;
		if (isSet($obj_output->itensObtidos) > 0) {

			foreach ($obj_output->conteudos as $key => $noticia) {
				
				isset($noticia->titulo) ? 
					$arr_noticias[$i]['titulo'] = $noticia->titulo : 
					$arr_noticias[$i]['titulo'] = '';
				isset($noticia->ultima_atualizacao) ? 
					$arr_noticias[$i]['datetime'] = \Carbon\Carbon::parse($noticia->ultima_atualizacao) : 
					$arr_noticias[$i]['datetime'] = '';
				isset($noticia->permalink) ? 
					$arr_noticias[$i]['link'] = $noticia->permalink : 
					$arr_noticias[$i]['link'] = '';
				isset($noticia->thumbnail) ? 
					$arr_noticias[$i]['image'] = $noticia->thumbnail : 
					$arr_noticias[$i]['image'] = '';

				$arr_noticias[$i]['site_id'] = 4;

				$i++;
			}
		}
		Log::info($arr_noticias);

		DB::beginTransaction();

		foreach ($arr_noticias as $noticia) {
			$n = \App\Noticia::where('titulo',$noticia['titulo'])->count();
			if ($n == 0) {
				\App\Noticia::create($noticia);
			}
		}

		DB::commit();

		Log::info('Rodou crawler g1');

		$this->info("Crawler g1");
	}

	/**
	 * Get the console command arguments.
	 *
	 * @return array
	 */
	protected function getArguments()
	{
		return [
			//['example', InputArgument::REQUIRED, 'An example argument.'],
		];
	}

	/**
	 * Get the console command options.
	 *
	 * @return array
	 */
	protected function getOptions()
	{
		return [
			//['example', null, InputOption::VALUE_OPTIONAL, 'An example option.', null],
		];
	}

}
