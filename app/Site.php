<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Site extends Model {

	protected $fillable = [
 		'titulo',
 		'link',
 		'categoria',
 		'data',
 		'hora',
 		'site_id',
 		'image_url'
 	];

}
