<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/
use Goutte\Client;
use \App\Noticia;
use \App\Site;

Route::get('/', 'WelcomeController@index');

Route::get('home', 'HomeController@index');

Route::controllers([
	'auth' => 'Auth\AuthController',
	'password' => 'Auth\PasswordController',
]);

Route::get('crawler', 'CrawlerController@crawler');

Route::get('crawler_g1', 'CrawlerController@crawler_g1');

Route::get('crawler_94fm', 'CrawlerController@crawler_94fm');

Route::get('crawler_96fm', 'CrawlerController@crawler_96fm');

Route::get('crawler_pref', 'CrawlerController@crawler_pref');

Route::get('social-bauru', 'CrawlerController@crawler_social');

Route::group(['middleware' => 'cors'], function(){

	Route::get('api', function(){

		$input = Input::all();
		$status = 200;
		$arrNoticias = [];
		isSet($input['page']) ? $skip = ($input['page'] - 1) * 10 : $skip = 0;

		try {

			$noticias = new Noticia;

			if (isSet($input['site']) and $input['site'] !== '0') {
				$noticias = $noticias->where('site_id', $input['site']);
			}

			if (isSet($input['keyword']) and $input['keyword'] !== '') {
				$noticias = $noticias->where('titulo', 'LIKE', "%".$input['keyword']."%");
			}

			$noticias = $noticias->skip($skip)
								->take(10)
								->orderBy('datetime', 'DESC')
								->get();

			foreach ($noticias as $key => $noticia) {
				$arrNoticias[] = [
					'titulo' => $noticia->titulo,
					'site'   => isSet($noticia->site->titulo) ? $noticia->site->titulo : "JC",
					'data'   => \Carbon\Carbon::parse($noticia->datetime)->format('d/m/Y H:i'),
					'link'   => $noticia->link,
					'image'  => isSet($noticia->image) ? $noticia->image : null,
					'site_image_url' => $noticia->site->image_url
				];
			}
		} catch (Exception $e) {
			$status = 500;
			$arrNoticias = ['error' => 'erro']; 
		}
		
		return response($arrNoticias, $status);
	});

	Route::get('api/fontes', function(){

		$fontes = Site::where('ativo',1)->get();

		return response($fontes, 200);
	});

	Route::get('imgs/{img}', function($img){

		$path = public_path('imgs/'.$img);

		if (file_exists($path)) {
	        return response()->download($path);
	    }
	});

});	