<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Goutte\Client;
use DB;
use App\Noticia;
use App\Site;
use \Carbon\Carbon;

class CrawlerController extends Controller {

	protected $client;
	protected $noticia;
	protected $site;

	public function __construct(Client $client, Noticia $noticia, Site $site){

		$this->client = $client;
		$this->noticia = $noticia;
		$this->site = $site;
	}

	public function crawler()
	{
		$client = new Client();

		$crawler = $client->request('GET', 'http://www.jcnet.com.br/ultimas.php');
		$noticias = array(); 
		$i = 0;
		$data = ''; 

		$array = $crawler->filter('table')->eq(7)->children()->each(function ($node) use ( &$noticias , &$i, &$data ){
			if (count($node->children()) == 1) {
				if ($node->children()->attr('valign') == 'bottom') {
				} elseif ($node->children()->attr('bgcolor') == '#E1ECFF') {
					$data = $node->children()->text();
				}
			} elseif (count($node->children()) == 3){
				$node->children()->each(function($td) use ( &$noticias, &$i, $data ){
					if ($td->attr('width') == '5%') {
						if (isSet($data)) {
							// $noticias[$i]['data'] = $data;
							$data = substr($data, 0, 8);
							$noticias[$i]['datetime'] = \Carbon\Carbon::createFromFormat('d/m/y H:i',$data.' '.$td->text());
						}
						// $noticias[$i]['hora'] = $td->text();
					} elseif ($td->attr('width') == '15%') {
						$noticias[$i]['categoria'] = $td->text();
					} elseif ($td->attr('width') == '80%') {
						$noticias[$i]['titulo'] = $td->text();
						$noticias[$i]['link'] = 'http://www.jcnet.com.br/'.$td->children()->attr('href');
					}						
				});
				$noticias[$i]['site_id'] = 1;
				$i++;
			}
		});

		foreach ($noticias as $noticia) {
			$n = \App\Noticia::where('titulo',$noticia['titulo'])->get();
			if (!count($n)) {
				\App\Noticia::create($noticia);
			}
		}

		return response()->json($noticias);
	}

	public function crawler_g1 ()
	{	

		$ch = curl_init();
	    curl_setopt($ch, CURLOPT_URL, 'http://g1.globo.com/dynamo/plantao/sp/bauru-marilia/1.json');
	    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); 
		$output = curl_exec($ch);

		curl_close($ch);

		$obj_output = json_decode($output);

		$arr_noticias = [];

		$i = 0;
		if (isSet($obj_output->itensObtidos) > 0) {

			foreach ($obj_output->conteudos as $key => $noticia) {
				
				isset($noticia->titulo) ? 
					$arr_noticias[$i]['titulo'] = $noticia->titulo : 
					$arr_noticias[$i]['titulo'] = '';
				isset($noticia->ultima_atualizacao) ? 
					$arr_noticias[$i]['datetime'] = \Carbon\Carbon::parse($noticia->ultima_atualizacao) : 
					$arr_noticias[$i]['datetime'] = '';
				isset($noticia->permalink) ? 
					$arr_noticias[$i]['link'] = $noticia->permalink : 
					$arr_noticias[$i]['link'] = '';
				isset($noticia->thumbnail) ? 
					$arr_noticias[$i]['image'] = $noticia->thumbnail : 
					$arr_noticias[$i]['image'] = '';

				$arr_noticias[$i]['site_id'] = 4;

				$i++;
			}
		}

		DB::beginTransaction();

		foreach ($arr_noticias as $noticia) {
			$n = \App\Noticia::where('titulo',$noticia['titulo'])->count();
			if ($n == 0) {
				\App\Noticia::create($noticia);
			}
		}

		DB::commit();

		return response()->json($arr_noticias);
	}

	public function crawler_94fm ()
	{
		$crawler = $this->client->request(
			'GET', 'http://94fm.com.br/ultimas-noticias/'
			);

		$arr_noticias = []; $i = 0;
		$crawler->filter('div#listaultimas ul')->children()->first()->children()->each(function ($node) use (&$arr_noticias, &$i){
		   	$arr_noticias[$i]['datetime'] = \Carbon\Carbon::createFromFormat('d/m/Y H:i', substr($node->filter('p.titulo span')->text(), 1, -1) .' '. date("H:i"));
		    $arr_noticias[$i]['link'] = $node->filter('p.titulo a')->attr('href'); //link
		    $arr_noticias[$i]['titulo'] = $node->filter('p')->eq(1)->text()."<br>"; //titulo
		    $i++;
		});

		DB::beginTransaction();

		foreach ($arr_noticias as $noticia) {
			$n = \App\Noticia::where('titulo',$noticia['titulo'])->count();
			if ($n == 0) {
				\App\Noticia::create($noticia);
			}
		}

		DB::commit();

		return response()->json($arr_noticias);
	}

	public function crawler_pref ()
	{
		$crawler = $this->client->request(
			'GET', 'http://www.bauru.sp.gov.br/noticias/noticias.aspx'
			);

		$crawler->filter('table#ctl00_ctl00_corpo_noticias_corpo_GridView1')->children()->each(function ($node) {
		    
			echo $node->filter('td.dataNoticia')->text();
			echo $node->filter('td')->eq(1)->children()->attr('href');
			echo $node->filter('td')->eq(1)->children()->text()."<br>";

		});
	}

	public function crawler_96fm ()
	{
		$crawler = $this->client->request(
			'GET', 'http://www.96fmbauru.com.br/noticias/Ultimas-Noticias'
		);

		$arrayNoticias = [];
		$i = 0;
		$data = '';

		$crawler->filter('div.noticia')->children()->each(function ($node) use (&$arrayNoticias, &$i, &$data) {
			if ($node->attr('class') == 'font14 fontPreto fontBold') {
				$data = $node->text();
			} elseif ($node->attr('class') == '') {
				$arrayNoticias[$i]['titulo'] = $node->text();
				$arrayNoticias[$i]['link'] = 'http://www.96fmbauru.com.br' . $node->children()->attr('href');
				$arrayNoticias[$i]['datetime'] = Carbon::createFromFormat('d/m/y H:i', $data .' '. date("H:i"));
				$arrayNoticias[$i]['site_id'] = 6;
				$i++;
			} 
		});

		DB::beginTransaction();

		foreach ($arrayNoticias as $noticia) {
			$n = $this->noticia->where('titulo',$noticia['titulo'])->count();
			if ($n == 0) {
				$this->noticia->create($noticia);
			}
		}

		DB::commit();
	}

	public function crawler_social ()
	{

		$crawler = $this->client->request(
			'GET', 'http://socialbauru.com.br/'
		);

		$arr_noticias = []; $i = 0; $data = '';

		$crawler->filter('div.social_noticias')->children()->each(function ($node) use ( &$arr_noticias , &$i, &$data ){
			if ($node->filter('div')->count()) {
				if ($node->text() == 'hoje') {
					$data = \Carbon\Carbon::now()->format('d/m/Y');
				} else {
					$data = $node->text();
				}
			} elseif ($node->filter('ul')->count()) {
				$node->filter('ul')->children()->each(function($li) use ( &$arr_noticias , &$i, &$data) {
					$arr_noticias[$i]['titulo'] = $li->children()->text();
					$arr_noticias[$i]['link'] = $li->children()->attr('href');
					$arr_noticias[$i]['datetime'] = \Carbon\Carbon::createFromFormat('d/m/Y H:i', $data .' '. date("H:i"));
					$arr_noticias[$i]['site_id'] = 5;
					$i++;
				});
			}
		});
	
		DB::beginTransaction();

		foreach ($arr_noticias as $noticia) {
			$n = \App\Noticia::where('titulo',$noticia['titulo'])->count();
			if ($n == 0) {
				\App\Noticia::create($noticia);
			}
		}

		DB::commit();
	}

}