<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Noticia extends Model {

 	protected $fillable = [
 		'titulo',
 		'link',
 		'categoria',
 		'data',
 		'hora',
 		'site_id',
 		'image',
 		'datetime',
 		'ativo',
 	];

 	public function site()
 	{
 		return $this->belongsTo('App\Site');
 	}
}
